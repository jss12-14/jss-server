const mainController = require("../controllers/main.controller.js");
const loginController = require("../controllers/login.controller.js");

module.exports = function (app, router) {
    app.use('/plants', loginController.verifyToken)
    app.route('/plants')
        .get(mainController.getPlant)
        .post(mainController.addPlantReport)
        .put(mainController.updatePlantReport)

    app.route('/users')
        .get(mainController.getUsers)

    app.use('/products', loginController.verifyToken)
    app.route('/products')
        .get(mainController.getProducts)
        .post(mainController.addMultipleProductReports)
        .put(mainController.updateProductReport)

    app.use('/subproducts', loginController.verifyToken)
    app.route('/subproducts')
        .get(mainController.getSubProducts)

    app.use('/customers', loginController.verifyToken)
    app.route('/customers')
        .get(mainController.getCustomers)
        .post(mainController.addMultipleCustomerReports)
        .put(mainController.updateCustomerReport)

    app.use('/benchmarks', loginController.verifyToken)
    app.route('/benchmarks')
        .get(mainController.getBenchmarks)

    app.use('/delete', loginController.verifyToken)
    app.route('/delete')
        .delete(mainController.deleteReport)

    app.use('/report', loginController.verifyToken)
    app.route('/report')
        .get(mainController.getReport)

    /* app.use('/reports', loginController.verifyToken)
    app.route('/reports')
        .get(mainController.getAllRecords) */

    app.use('/checkreports', loginController.verifyToken)
    app.route('/checkreports')
        .get(mainController.checkReportsPresentForYear)

}