const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const lo = require('lodash');
const mainController = require("./main.controller.js");
let conObj = require("../../_config")
let Connection = require('tedious').Connection;
let Request = require('tedious').Request;

exports.login = function (req, res) {
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            var request = new Request(
                "SELECT User_ID, User_Name, Status FROM \"User\" WHERE ROLE = 'PM' AND USER_NAME = '" + req.body.username + "' AND Password = '" + req.body.password + "'",
                function (err) {
                    if (err) {
                        console.log("Ignore error")
                    }
                }
            );
            connection.execSql(request);
            var data = {};
            //connection.execSql(request);
            request.on('row', function (columns) {
                columns.forEach(function (column) {
                    if (column.value === null) {
                        console.log('NULL');
                    } else {
                        data[column.metadata.colName] = column.value;
                    }
                });
            });

            request.on('doneInProc', function (rowCount, more) {
                if (rowCount) {
                    new Promise(function (resolve, reject) {
                        createToken(data)
                            .then((resp) => {
                                resolve(resp)
                            })
                            .catch((err) => {
                                reject(err)
                            })
                    })
                        .then((data) => {
                            res.status(200).send(data)
                        })
                }
                else {
                    msg = "Please try again later";
                    res.status(401).send(msg)
                }
            });

            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}

createToken = function (userObj) {
    this.authData = {
        "LoginId": userObj.User_ID,
        "referenceID": crypto.randomBytes(16).toString('hex')
    }
    var payload = lo.pick(this.authData, ['LoginId', 'createdAt']);
    return new Promise(function (fulfill, reject) {
        jwt.sign(payload, 'jssSecretKey', (err, token) => { //createHash(this.authData.referenceID)
            userObj["token"] = token
            fulfill(userObj);
        });
    })
}

/* createHash = function (data) {
    return crypto.createHash('md5').update(data + data.substring(0, 10)).digest("hex");
}; */

exports.verifyToken = function (req, res, next) {
    if (req.method === 'OPTIONS') {
        console.log('!OPTIONS');
        var headers = {};
        // IE8 does not allow domains to be specified, just the *
        // headers["Access-Control-Allow-Origin"] = req.headers.origin;
        headers["Access-Control-Allow-Origin"] = "*";
        headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
        headers["Access-Control-Allow-Credentials"] = false;
        headers["Access-Control-Max-Age"] = '86400'; // 24 hours
        headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization";
        res.writeHead(200, headers);
        res.end();
    } else {
        const bearerHeader = req.headers['authorization'];
        //console.log("bearerHeader", bearerHeader)
        if (typeof bearerHeader !== 'undefined') {
            const bearer = bearerHeader.split(' ');
            const bearerToken = bearer[1];
            //console.log("bearerToken", bearerToken)
            jwt.verify(bearerToken, 'jssSecretKey', function (err, authData) {
                if (err) {
                    res.sendStatus(401);
                }
                else {
                    //console.log("authData", authData);
                    mainController.setAuthdata(authData)
                    next();
                }
            })


        }
        else {
            res.sendStatus(401);
        }
    }
};
