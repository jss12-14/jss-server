let conObj = require("../../_config")
/**** to establish connection to database and get db object **/
let Connection = require('tedious').Connection;
let Request = require('tedious').Request;
let TYPES = require('tedious').TYPES;
/* exports.config = function (con) {
    connection = con;
} */
/***********************Authdata from token********************/
let authdata;
exports.setAuthdata = function (data) {
    authdata = data;
}
/*************************************************************/

exports.getUsers = function (req, res) {
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            var request = new Request(
                "SELECT User_ID, User_Name, Status FROM \"User\"",
                function (err) {
                    if (err) {
                        res.status(500).send(err);
                    }
                }
            );

            var data = {};
            var result = [];
            connection.execSql(request);
            await request.on('row', function (columns) {
                columns.forEach(function (column) {
                    if (column.value === null) {
                        console.log('NULL');
                    } else {
                        data[column.metadata.colName] = column.value;
                    }
                });
                result.push(data);
                data = {};
            });

            request.on('doneInProc', function (rowCount, more) {
                if (rowCount) {
                    res.status(200).send(result)
                }
                else {
                    msg = "Please try again later";
                    res.status(500).send(msg)
                }
            });
            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}

exports.getPlant = async function (req, res) {
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            id = authdata.LoginId;
            let query;
            query = "SELECT * from  View_Plant_DropDown WHERE Status = 'A' AND (Plant_Manager = '" + id + "' OR Regional_Manager = '" + id + "')"
            var request = new Request(
                query,
                function (err) {
                    if (err) {
                        res.send(err);
                    }
                }
            );

            var data = {};
            var result = [];
            connection.execSql(request);
            await request.on('row', function (columns) {
                columns.forEach(function (column) {
                    if (column.value === null) {
                        console.log('NULL');
                    } else {
                        data[column.metadata.colName] = column.value;
                    }
                });
                result.push(data);
                data = {};
            });

            request.on('doneInProc', function (rowCount, more) {
                if (rowCount) {
                    res.status(200).send(result)
                }
                else {
                    msg = "Please try again later";
                    res.status(500).send(msg)
                }
            });

            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}

exports.getProducts = async function (req, res) {
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            var request = new Request(
                "SELECT Prod_ID, Prod_Type FROM Product where Status = 'A'",
                function (err) {
                    if (err) {
                        res.status(500).send(err);
                    }
                }
            );

            var data = {};
            var result = [];
            connection.execSql(request);
            await request.on('row', function (columns) {
                columns.forEach(function (column) {
                    if (column.value === null) {
                        console.log('NULL');
                    } else {
                        data[column.metadata.colName] = column.value;
                    }
                });
                result.push(data);
                data = {};
                //console.log("result")
            });

            request.on('doneInProc', function (rowCount, more) {
                //console.log("data",result)
                //console.log(rowCount + ' rows returned');
                if (rowCount) {
                    res.status(200).send(result)
                }
                else {
                    msg = "Please try again later";
                    res.status(500).send(msg)
                }
            });

            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}

exports.getSubProducts = async function (req, res) {
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            var request = new Request(
                "SELECT Sub_Prod_ID, Sub_Prod_Type FROM Sub_Product where Status = 'A'",
                function (err) {
                    if (err) {
                        res.status(500).send(err);
                    }
                }
            );

            var data = {};
            var result = [];
            connection.execSql(request);
            await request.on('row', function (columns) {
                columns.forEach(function (column) {
                    if (column.value === null) {
                        console.log('NULL');
                    } else {
                        data[column.metadata.colName] = column.value;
                    }
                });
                result.push(data);
                data = {};
                //console.log("result")
            });

            request.on('doneInProc', function (rowCount, more) {
                //console.log("data",result)
                //console.log(rowCount + ' rows returned');
                if (rowCount) {
                    res.status(200).send(result)
                }
                else {
                    msg = "Please try again later";
                    res.status(500).send(msg)
                }
            });

            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}


exports.getCustomers = async function (req, res) {
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            var request = new Request(
                "SELECT Cust_ID, Cust_Name FROM Customer WHERE Status = 'A'",
                function (err) {
                    if (err) {
                        res.status(500).send(err);
                    }
                }
            );

            var data = {};
            var result = [];
            connection.execSql(request);
            await request.on('row', function (columns) {
                columns.forEach(function (column) {
                    if (column.value === null) {
                        console.log('NULL');
                    } else {
                        data[column.metadata.colName] = column.value;
                    }
                });
                result.push(data);
                data = {};
                //console.log("result")
            });

            request.on('doneInProc', function (rowCount, more) {
                if (rowCount) {
                    res.status(200).send(result)
                }
                else {
                    msg = "Please try again later";
                    res.status(500).send(msg)
                }
            });

            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}

exports.getBenchmarks = async function (req, res) {
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            var request = new Request(
                "SELECT Benchmark_ID, Benchmark_Name FROM Benchmark WHERE Status = 'A'",
                function (err) {
                    if (err) {
                        res.status(500).send(err);
                    }
                }
            );

            var data = {};
            var result = [];
            connection.execSql(request);
            await request.on('row', function (columns) {
                columns.forEach(function (column) {
                    if (column.value === null) {
                        console.log('NULL');
                    } else {
                        data[column.metadata.colName] = column.value;
                    }
                });
                result.push(data);
                data = {};
            });

            request.on('doneInProc', function (rowCount, more) {
                if (rowCount) {
                    res.status(200).send(result)
                }
                else {
                    msg = "Please try again later";
                    res.status(500).send(msg)
                }
            });

            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}

exports.checkReportsPresentForYear = async function (req, res) {
    year = req.query.year
    plantId = req.query.plantId
    //console.log("year", year);
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            var request = new Request(
                "Are_Records_Presents_For_Year",
                function (err) {
                    if (err) {
                        res.status(500).send(err);
                    }
                }
            );
            request.addParameter('year', TYPES.Int, year);
            request.addParameter('plantId', TYPES.VarChar, plantId);

            var data = {};
            var result = [];

            connection.callProcedure(request);
            request.on('row', function (columns) {
                columns.forEach(function (column) {
                    if (column.value === null) {
                        console.log('NULL');
                    } else {
                        //console.log("column name" + column.metadata.colName + "column value", column.value)
                        data[column.metadata.colName] = column.value;
                    }
                });
                result.push(data);
                data = {};
            });
            request.on('requestCompleted', async function () {
                if (result) {
                    res.status(200).send(result)
                }
                else {
                    msg = "Please try again later";
                    res.status(500).send(msg)
                }
                connection.close()
            });
        }
    });
}

exports.getReport = async function (req, res) {
    data = {}
    let date = req.query.date;
    let plantId = req.query.plantId;
    data["plant"] = await exports.getPlantReport(date, plantId)
    data["customer"] = await exports.getCustomerReport(date, plantId)
    data["product"] = await exports.getProductReport(date, plantId)
    res.send(data)
}

exports.getPlantReport = function (date, plantId) {
    return new Promise((resolve, reject) => {
        var connection = new Connection(conObj.config);
        connection.on('connect', async function (err) {
            if (err) {
                //res.status(500).send(err);
                console.log("err", err)
            }
            else {
                //"select * from Plant_KPI as t1,Product_KPI as t2,Customer_KPI as t3 where t1.Created_Date = '" + date + "' and t1.Pk_ID = t2.Pdt_ID and t1.Pk_ID = t3.Ck_ID"
                var request = new Request(
                    "select * from Plant_KPI where Created_Date = '" + date + "' and Plant_ID ='" + plantId + "'",
                    function (err) {
                        if (err) {
                            reject(err);
                        }
                    }
                );

                var data = {};
                var result = [];
                connection.execSql(request);
                await request.on('row', function (columns) {
                    columns.forEach(function (column) {
                        data[column.metadata.colName] = column.value;
                        //console.log("data ===>", column.metadata.colName, " - ", column.value)
                    });
                    result.push(data);
                    data = {};
                });

                request.on('doneInProc', function () {
                    resolve(result);
                });

                request.on('requestCompleted', function () {
                    connection.close()
                });
            }
        });

    })
}

exports.getCustomerReport = function (date, plantId) {
    return new Promise((resolve, reject) => {
        var connection = new Connection(conObj.config);
        connection.on('connect', async function (err) {
            if (err) {
                //res.status(500).send(err);
                reject(err);
            }
            else {
                //"select * from Plant_KPI as t1,Product_KPI as t2,Customer_KPI as t3 where t1.Created_Date = '" + date + "' and t1.Pk_ID = t2.Pdt_ID and t1.Pk_ID = t3.Ck_ID"
                var request = new Request(
                    "select * from Customer_KPI where Created_Date like '" + date + "' and Plant_ID ='" + plantId + "'",
                    function (err) {
                        if (err) {
                            reject(err);
                        }
                    }
                );

                var data = {};
                var result = [];
                connection.execSql(request);
                await request.on('row', function (columns) {
                    columns.forEach(function (column) {
                        data[column.metadata.colName] = column.value;
                        /* if (column.value === null) {
                            console.log('NULL');
                        } else {
                            data[column.metadata.colName] = column.value;
                            //console.log("data ===>", column.metadata.colName, " - ", column.value)
                        } */
                    });
                    result.push(data);
                    data = {};
                });

                request.on('doneInProc', function () {
                    resolve(result);
                });

                request.on('requestCompleted', function () {
                    connection.close()
                });
            }
        });

    })
}

exports.getProductReport = function (date, plantId) {
    return new Promise((resolve, reject) => {
        var connection = new Connection(conObj.config);
        connection.on('connect', async function (err) {
            if (err) {
                //res.status(500).send(err);
                reject(err);
            }
            else {
                //"select * from Plant_KPI as t1,Product_KPI as t2,Customer_KPI as t3 where t1.Created_Date = '" + date + "' and t1.Pk_ID = t2.Pdt_ID and t1.Pk_ID = t3.Ck_ID"
                var request = new Request(
                    "select * from Product_KPI where Created_Date like '" + date + "' and Plant_ID = '" + plantId + "'",
                    function (err) {
                        if (err) {
                            reject(err);
                        }
                    }
                );

                var data = {};
                var result = [];
                connection.execSql(request);
                await request.on('row', function (columns) {
                    columns.forEach(function (column) {
                        data[column.metadata.colName] = column.value;
                    });
                    result.push(data);
                    data = {};
                });

                request.on('doneInProc', function () {
                    resolve(result);
                });

                request.on('requestCompleted', function () {
                    connection.close()
                });
            }
        });

    })
}

exports.addPlantReport = function (req, res) {
    var connection = new Connection(conObj.config);
    var error = 0;
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            var request = new Request(
                "INSERT INTO Plant_KPI (Plant_ID,Benchmark_ID,User_ID,Tot_Emp_First_Day_Mon,Tot_Emp_Last_Day_Mon,Tot_Lost_Time_Days_Mon,Lost_Time_Incident,Created_Date,Updated_Date,Reported_Date) VALUES (@Plant_ID,@Benchmark_ID,@User_ID,@Tot_Emp_First_Day_Mon,@Tot_Emp_Last_Day_Mon,@Tot_Lost_Time_Days_Mon,@Lost_Time_Incident,@Created_Date,@Updated_Date,@Reported_Date);",
                function (err) {
                    if (err) {
                        error = err
                    }
                }
            );

            //request.addParameter('Pk_ID', TYPES.VarChar, req.body.Pk_ID);
            request.addParameter('Plant_ID', TYPES.VarChar, req.body.Plant_ID);
            request.addParameter('Benchmark_ID', TYPES.VarChar, req.body.Benchmark_ID);
            request.addParameter('User_ID', TYPES.VarChar, req.body.User_ID);
            request.addParameter('Tot_Emp_First_Day_Mon', TYPES.Int, req.body.Tot_Emp_First_Day_Mon);
            request.addParameter('Tot_Emp_Last_Day_Mon', TYPES.Int, req.body.Tot_Emp_Last_Day_Mon);
            request.addParameter('Tot_Lost_Time_Days_Mon', TYPES.Int, req.body.Tot_Lost_Time_Days_Mon);
            request.addParameter('Lost_Time_Incident', TYPES.Int, req.body.Lost_Time_Incident);
            request.addParameter('Created_Date', TYPES.Date, req.body.Created_Date);
            request.addParameter('Updated_Date', TYPES.Date, new Date());
            request.addParameter('Reported_Date', TYPES.Date, new Date());

            connection.execSql(request);
            request.on('requestCompleted', function () {
                msg = "Success"
                if (error) {
                    res.status(500).send(error);
                }
                else {
                    res.status(200).send(msg)
                }
            });

            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}

exports.addMultipleCustomerReports = async function (req, res) {
    var error = 0;
    for (i = 0; i < req.body.length; i++) {
        await addCustomerReport(req.body[i])
            .then(() => {
                console.log(i + 1, " row inserted to customer")
            })
            .catch((err) => {
                error = err
            })
        if (error) {
            break;
        }
    }
    if (error) {
        console.log("error", error)
        res.status(400).send("Duplicate")
    }
    else {
        res.sendStatus(200)
    }
}

addCustomerReport = function (data) {
    return new Promise((resolve, reject) => {
        //console.log("data",data)
        var error = 0;
        var connection = new Connection(conObj.config);
        connection.on('connect', async function (err) {
            if (err) {
                //res.status(500).send(err);
                reject(err)
            }
            else {
                var request = new Request(
                    "INSERT INTO Customer_KPI (Plant_ID,Benchmark_ID,Prod_ID,Cust_ID,User_ID,Sub_Prod_ID,Cnf_Prod_Shipped_Mon,Tot_Prod_Shipped_Mon,Revenue_Mon,Tot_Rjctd_Qty_For_Mon,Shipped_Qty_For_Mon,Scrap_For_Mon,No_Of_Complaints,Created_Date,Updated_Date,Reported_Date) VALUES (@Plant_ID,@Benchmark_ID,@Prod_ID,@Cust_ID,@User_ID,@Sub_Prod_ID,@Cnf_Prod_Shipped_Mon,@Tot_Prod_Shipped_Mon,@Revenue_Mon,@Tot_Rjctd_Qty_For_Mon,@Shipped_Qty_For_Mon,@Scrap_For_Mon,@No_Of_Complaints,@Created_Date,@Updated_Date,@Reported_Date);",
                    function (err) {
                        if (err) {
                            error = err
                        }
                    }
                );

                //request.addParameter('Ck_ID', TYPES.VarChar, data.Ck_ID);
                request.addParameter('Plant_ID', TYPES.VarChar, data.Plant_ID);
                request.addParameter('Benchmark_ID', TYPES.VarChar, data.Benchmark_ID);
                request.addParameter('Prod_ID', TYPES.VarChar, data.Prod_ID);
                request.addParameter('Cust_ID', TYPES.VarChar, data.Cust_ID);
                request.addParameter('User_ID', TYPES.VarChar, data.User_ID);
                request.addParameter('Sub_Prod_ID', TYPES.VarChar, data.Sub_Prod_ID);
                request.addParameter('Cnf_Prod_Shipped_Mon', TYPES.Float, data.Cnf_Prod_Shipped_Mon);
                request.addParameter('Tot_Prod_Shipped_Mon', TYPES.Float, data.Tot_Prod_Shipped_Mon);
                request.addParameter('Revenue_Mon', TYPES.Float, data.Revenue_Mon);
                request.addParameter('Tot_Rjctd_Qty_For_Mon', TYPES.Float, data.Tot_Rjctd_Qty_For_Mon);
                request.addParameter('Shipped_Qty_For_Mon', TYPES.Float, data.Shipped_Qty_For_Mon);
                request.addParameter('Scrap_For_Mon', TYPES.Float, data.Scrap_For_Mon);
                request.addParameter('No_Of_Complaints', TYPES.Int, data.No_Of_Complaints);
                request.addParameter('Created_Date', TYPES.Date, data.Created_Date);
                request.addParameter('Updated_Date', TYPES.Date, new Date());
                request.addParameter('Reported_Date', TYPES.Date, new Date());

                connection.execSql(request);
                request.on('requestCompleted', function () {
                    msg = "Success"
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(msg)
                    }
                });

                request.on('requestCompleted', function () {
                    connection.close()
                });
            }
        });
    })
}

exports.addMultipleProductReports = async function (req, res) {
    var error = 0
    for (j = 0; j < req.body.length; j++) {
        await addProductReport(req.body[j])
            .then(() => {
                console.log(j + 1, " row inserted to product")
            })
            .catch((err) => {
                error = err
            })
        if (error) {
            break;
        }
    }
    if (error) {
        console.log("error", error)
        res.status(400).send("Duplicate")
    }
    else {
        res.sendStatus(200)
    }
}

addProductReport = function (data) {
    return new Promise((resolve, reject) => {
        var error = 0;
        var connection = new Connection(conObj.config);
        connection.on('connect', async function (err) {
            if (err) {
                reject(err);
                //res.status(500).send(err);
            }
            else {
                var request = new Request(
                    "INSERT INTO Product_KPI (Plant_ID,Benchmark_ID,User_ID,Prod_ID,Emp_Left_Mon,Tot_Emp_First_Day_Mon,Tot_Emp_Last_Day_Mon,Tot_Supp_PPM_Qlty,Cost_Premium_Freight_In_Mon,Cost_Premium_Freight_Out_Mon,Revenue_Mon,Cogs,Mon_End_Inv_Local_Curr,Pre_Mon_End_Inv_Local_Curr,All_Mfg_Cost_PnL,Overtime_Cost_PnL,Created_Date,Updated_Date,Reported_Date) VALUES (@Plant_ID,@Benchmark_ID,@User_ID,@Prod_ID,@Emp_Left_Mon,@Tot_Emp_First_Day_Mon,@Tot_Emp_Last_Day_Mon,@Tot_Supp_PPM_Qlty,@Cost_Premium_Freight_In_Mon,@Cost_Premium_Freight_Out_Mon,@Revenue_Mon,@Cogs,@Mon_End_Inv_Local_Curr,@Pre_Mon_End_Inv_Local_Curr,@All_Mfg_Cost_PnL,@Overtime_Cost_PnL,@Created_Date,@Updated_Date,@Reported_Date);",
                    function (err) {
                        if (err) {
                            error = err
                        }
                    }
                );

                //request.addParameter('Pdt_ID', TYPES.VarChar, data.Pdt_ID);
                request.addParameter('Plant_ID', TYPES.VarChar, data.Plant_ID);
                request.addParameter('Benchmark_ID', TYPES.VarChar, data.Benchmark_ID);
                request.addParameter('User_ID', TYPES.VarChar, data.User_ID);
                request.addParameter('Prod_ID', TYPES.VarChar, data.Prod_ID);
                request.addParameter('Emp_Left_Mon', TYPES.Int, data.Emp_Left_Mon);
                request.addParameter('Tot_Emp_First_Day_Mon', TYPES.Int, data.Tot_Emp_First_Day_Mon);
                request.addParameter('Tot_Emp_Last_Day_Mon', TYPES.Int, data.Tot_Emp_Last_Day_Mon);
                request.addParameter('Tot_Supp_PPM_Qlty', TYPES.Float, data.Tot_Supp_PPM_Qlty);
                request.addParameter('Cost_Premium_Freight_In_Mon', TYPES.Float, data.Cost_Premium_Freight_In_Mon);
                request.addParameter('Cost_Premium_Freight_Out_Mon', TYPES.Float, data.Cost_Premium_Freight_Out_Mon);
                request.addParameter('Revenue_Mon', TYPES.Float, data.Revenue_Mon);
                request.addParameter('Cogs', TYPES.Float, data.Cogs);
                request.addParameter('Mon_End_Inv_Local_Curr', TYPES.Float, data.Mon_End_Inv_Local_Curr);
                request.addParameter('Pre_Mon_End_Inv_Local_Curr', TYPES.Float, data.Pre_Mon_End_Inv_Local_Curr);
                request.addParameter('All_Mfg_Cost_PnL', TYPES.Float, data.All_Mfg_Cost_PnL);
                request.addParameter('Overtime_Cost_PnL', TYPES.Float, data.Overtime_Cost_PnL);
                request.addParameter('Created_Date', TYPES.Date, data.Created_Date);
                request.addParameter('Updated_Date', TYPES.Date, new Date());
                request.addParameter('Reported_Date', TYPES.Date, new Date());

                connection.execSql(request);
                request.on('requestCompleted', function () {
                    msg = "Success"
                    if (error) {
                        reject(error);
                    }
                    else {
                        resolve(msg)
                    }
                });

                request.on('requestCompleted', function () {
                    connection.close()
                });
            }
        });
    })
}

exports.deleteReport = async function (req, res) {
    let date = req.query.date;
    let plantId = req.query.plantId;
    let error = 0;
    let msg
    await deletePlantReport(date, plantId)
        .then((data) => {
            msg = data
        })
        .catch((err) => {
            console.log("Error at delete plant", err)
            error = err
        })
    await deleteCustomerReport(date, plantId)
        .then((data) => {
            msg = data
        })
        .catch((err) => {
            console.log("Error at delete customer", err)
            error = err
        })
    await deleteProductReport(date, plantId)
        .then((data) => {
            msg = data
        })
        .catch((err) => {
            console.log("Error at delete product", err)
            error = err
        })
    if (error) {
        res.sendStatus(500)
    }
    else {
        res.status(200).send(msg)
    }
}

deletePlantReport = function (date, plantId) {
    return new Promise((resolve, reject) => {
        var error = 0;
        var connection = new Connection(conObj.config);
        connection.on('connect', async function (err) {
            if (err) {
                reject(err);
            }
            else {
                var request = new Request(
                    "Delete from Plant_KPI where Created_Date = '" + date + "' and Plant_ID ='" + plantId + "'",
                    function (err) {
                        if (err) {
                            error = err
                        }
                    }
                );

                connection.execSql(request);
                request.on('doneInProc', function (rowCount, more, rows) {
                    if (error) {
                        reject(error);
                    }
                    else if (rowCount) {
                        msg = "Success"
                        resolve(msg)
                    }
                    else {
                        msg = "No rows to delete"
                        resolve(msg)
                    }
                });

                request.on('requestCompleted', function () {
                    connection.close()
                });
            }
        });
    })
}

deleteCustomerReport = function (date, plantId) {
    return new Promise((resolve, reject) => {
        var error = 0;
        var connection = new Connection(conObj.config);
        connection.on('connect', async function (err) {
            if (err) {
                reject(err);
            }
            else {
                var request = new Request(
                    "Delete from Customer_KPI where Created_Date = '" + date + "' and Plant_ID ='" + plantId + "'",
                    function (err) {
                        if (err) {
                            error = err
                        }
                    }
                );

                connection.execSql(request);
                request.on('doneInProc', function (rowCount, more, rows) {
                    if (error) {
                        reject(error);
                    }
                    else if (rowCount) {
                        msg = "Success"
                        resolve(msg)
                    }
                    else {
                        msg = "No rows to delete"
                        resolve(msg)
                    }
                });

                request.on('requestCompleted', function () {
                    connection.close()
                });
            }
        });
    })
}

deleteProductReport = function (date, plantId) {
    return new Promise((resolve, reject) => {
        var error = 0;
        var connection = new Connection(conObj.config);
        connection.on('connect', async function (err) {
            if (err) {
                reject(err);
            }
            else {
                var request = new Request(
                    "Delete from Product_KPI where Created_Date = '" + date + "' and Plant_ID ='" + plantId + "'",
                    function (err) {
                        if (err) {
                            error = err
                        }
                    }
                );

                connection.execSql(request);
                request.on('doneInProc', function (rowCount, more, rows) {
                    if (error) {
                        reject(error);
                    }
                    else if (rowCount) {
                        msg = "Success"
                        resolve(msg)
                    }
                    else {
                        msg = "No rows to delete"
                        resolve(msg)
                    }
                });

                request.on('requestCompleted', function () {
                    connection.close()
                });
            }
        });
    })
}

exports.updatePlantReport = function (req, res) {
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            var request = new Request(
                "Update Plant_KPI set Plant_ID = @Plant_ID,Benchmark_ID = @Benchmark_ID,User_ID= @User_ID,Tot_Emp_First_Day_Mon = @Tot_Emp_First_Day_Mon,Tot_Emp_Last_Day_Mon = @Tot_Emp_Last_Day_Mon,Tot_Lost_Time_Days_Mon = @Tot_Lost_Time_Days_Mon,Lost_Time_Incident=@Lost_Time_Incident,Created_Date=@Created_Date,Updated_Date=@Updated_Date WHERE Pk_ID = @Pk_ID",
                function (err) {
                    if (err) {
                        res.status(500).send(err);
                    }
                }
            );

            request.addParameter('Pk_ID', TYPES.VarChar, req.body.Pk_ID);
            request.addParameter('Plant_ID', TYPES.VarChar, req.body.Plant_ID);
            request.addParameter('Benchmark_ID', TYPES.VarChar, req.body.Benchmark_ID);
            request.addParameter('User_ID', TYPES.VarChar, req.body.User_ID);
            request.addParameter('Tot_Emp_First_Day_Mon', TYPES.Int, req.body.Tot_Emp_First_Day_Mon);
            request.addParameter('Tot_Emp_Last_Day_Mon', TYPES.Int, req.body.Tot_Emp_Last_Day_Mon);
            request.addParameter('Tot_Lost_Time_Days_Mon', TYPES.Int, req.body.Tot_Lost_Time_Days_Mon);
            request.addParameter('Lost_Time_Incident', TYPES.Int, req.body.Lost_Time_Incident);
            request.addParameter('Created_Date', TYPES.Date, req.body.Created_Date);
            request.addParameter('Updated_Date', TYPES.Date, req.body.Updated_Date);

            connection.execSql(request);
            request.on('doneInProc', function (rowCount, more, rows) {
                if (rowCount) {
                    msg = "Success"
                    res.status(200).send(msg)
                }
                else {
                    msg = "Please try again with correct input"
                    res.status(400).send(msg)
                }
            });

            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}

exports.updateCustomerReport = function (req, res) {
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            var request = new Request(
                "Update Customer_KPI set Plant_ID=@Plant_ID,Benchmark_ID=@Benchmark_ID,Prod_ID=@Prod_ID,Cust_ID=@Cust_ID,User_ID=@User_ID,Cnf_Prod_Shipped_Mon=@Cnf_Prod_Shipped_Mon,Tot_Prod_Shipped_Mon=@Tot_Prod_Shipped_Mon,Revenue_Mon=@Revenue_Mon,Tot_Rjctd_Qty_For_Mon=@Tot_Rjctd_Qty_For_Mon,Shipped_Qty_For_Mon=@Shipped_Qty_For_Mon,Scrap_For_Mon=@Scrap_For_Mon,No_Of_Complaints=@No_Of_Complaints,Created_Date=@Created_Date,Updated_Date=@Updated_Date WHERE Ck_ID=@Ck_ID;",
                function (err) {
                    if (err) {
                        res.status(500).send(err);
                    }
                }
            );

            request.addParameter('Ck_ID', TYPES.VarChar, req.body.Ck_ID);
            request.addParameter('Plant_ID', TYPES.VarChar, req.body.Plant_ID);
            request.addParameter('Benchmark_ID', TYPES.VarChar, req.body.Benchmark_ID);
            request.addParameter('Prod_ID', TYPES.VarChar, req.body.Prod_ID);
            request.addParameter('Cust_ID', TYPES.VarChar, req.body.Cust_ID);
            request.addParameter('User_ID', TYPES.VarChar, req.body.User_ID);
            request.addParameter('Cnf_Prod_Shipped_Mon', TYPES.Decimal, req.body.Cnf_Prod_Shipped_Mon);
            request.addParameter('Tot_Prod_Shipped_Mon', TYPES.Decimal, req.body.Tot_Prod_Shipped_Mon);
            request.addParameter('Revenue_Mon', TYPES.Decimal, req.body.Revenue_Mon);
            request.addParameter('Tot_Rjctd_Qty_For_Mon', TYPES.Decimal, req.body.Tot_Rjctd_Qty_For_Mon);
            request.addParameter('Shipped_Qty_For_Mon', TYPES.Decimal, req.body.Shipped_Qty_For_Mon);
            request.addParameter('Scrap_For_Mon', TYPES.Decimal, req.body.Scrap_For_Mon);
            request.addParameter('No_Of_Complaints', TYPES.Int, req.body.No_Of_Complaints);
            request.addParameter('Created_Date', TYPES.Date, req.body.Created_Date);
            request.addParameter('Updated_Date', TYPES.Date, req.body.Updated_Date);

            connection.execSql(request);
            request.on('doneInProc', function (rowCount, more, rows) {
                if (rowCount) {
                    msg = "Success"
                    res.status(200).send(msg)
                }
                else {
                    msg = "Please try again with correct input"
                    res.status(400).send(msg)
                }
            });

            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}

exports.updateProductReport = function (req, res) {
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            var request = new Request(
                "Update Product_KPI set Plant_ID=@Plant_ID,Benchmark_ID=@Benchmark_ID,User_ID=@User_ID,Prod_ID=@Prod_ID,Emp_Left_Mon=@Emp_Left_Mon,Tot_Emp_First_Day_Mon=@Tot_Emp_First_Day_Mon,Tot_Emp_Last_Day_Mon=@Tot_Emp_Last_Day_Mon,Tot_Supp_PPM_Qlty=@Tot_Supp_PPM_Qlty,Cost_Premium_Freight_In_Mon=@Cost_Premium_Freight_In_Mon,Cost_Premium_Freight_Out_Mon=@Cost_Premium_Freight_Out_Mon,Revenue_Mon=@Revenue_Mon,Cogs=@Cogs,Mon_End_Inv_Local_Curr=@Mon_End_Inv_Local_Curr,Pre_Mon_End_Inv_Local_Curr=@Pre_Mon_End_Inv_Local_Curr,All_Mfg_Cost_PnL=@All_Mfg_Cost_PnL,Overtime_Cost_PnL=@Overtime_Cost_PnL,Created_Date=@Created_Date,Updated_Date=@Updated_Date WHERE Pdt_ID =@Pdt_ID;",
                function (err) {
                    if (err) {
                        res.status(500).send(err);
                    }
                }
            );

            request.addParameter('Pdt_ID', TYPES.VarChar, req.body.Pdt_ID);
            request.addParameter('Plant_ID', TYPES.VarChar, req.body.Plant_ID);
            request.addParameter('Benchmark_ID', TYPES.VarChar, req.body.Benchmark_ID);
            request.addParameter('User_ID', TYPES.VarChar, req.body.User_ID);
            request.addParameter('Prod_ID', TYPES.VarChar, req.body.Prod_ID);
            request.addParameter('Emp_Left_Mon', TYPES.Int, req.body.Emp_Left_Mon);
            request.addParameter('Tot_Emp_First_Day_Mon', TYPES.Int, req.body.Tot_Emp_First_Day_Mon);
            request.addParameter('Tot_Emp_Last_Day_Mon', TYPES.Int, req.body.Tot_Emp_Last_Day_Mon);
            request.addParameter('Tot_Supp_PPM_Qlty', TYPES.Decimal, req.body.Tot_Supp_PPM_Qlty);
            request.addParameter('Cost_Premium_Freight_In_Mon', TYPES.Decimal, req.body.Cost_Premium_Freight_In_Mon);
            request.addParameter('Cost_Premium_Freight_Out_Mon', TYPES.Decimal, req.body.Cost_Premium_Freight_Out_Mon);
            request.addParameter('Revenue_Mon', TYPES.Decimal, req.body.Revenue_Mon);
            request.addParameter('Cogs', TYPES.Decimal, req.body.Cogs);
            request.addParameter('Mon_End_Inv_Local_Curr', TYPES.Decimal, req.body.Mon_End_Inv_Local_Curr);
            request.addParameter('Pre_Mon_End_Inv_Local_Curr', TYPES.Decimal, req.body.Pre_Mon_End_Inv_Local_Curr);
            request.addParameter('All_Mfg_Cost_PnL', TYPES.Int, req.body.All_Mfg_Cost_PnL);
            request.addParameter('Overtime_Cost_PnL', TYPES.Int, req.body.Overtime_Cost_PnL);
            request.addParameter('Created_Date', TYPES.Date, req.body.Created_Date);
            request.addParameter('Updated_Date', TYPES.Date, req.body.Updated_Date);

            connection.execSql(request);
            request.on('doneInProc', function (rowCount, more, rows) {
                if (rowCount) {
                    msg = "Success"
                    res.status(200).send(msg)
                }
                else {
                    msg = "Please try again with correct input"
                    res.status(400).send(msg)
                }
            });

            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}


/*

exports.getAllRecords = async function (req, res) {
    var connection = new Connection(conObj.config);
    connection.on('connect', async function (err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            if (req.query.year) {
                toDate = ((new Date(req.query.year, 11, 31)).toISOString()).substr(0, 10)
                fromDate = ((new Date(req.query.year, 0, 1)).toISOString()).substr(0, 10)
            }
            else {
                fromDate = '2018-01-01';
                toDate = '2018-12-31';
            }
            var request = new Request(
                "select * from Plant_KPI as t1,Product_KPI as t2,Customer_KPI as t3 where t1.Pk_ID = t2.Pdt_ID and t1.Pk_ID = t3.Ck_ID and (t1.Created_Date BETWEEN '" + fromDate + "'AND '" + toDate + "')",
                function (err) {
                    if (err) {
                        res.status(500).send(err);
                    }
                }
            );

            var data = {};
            var result = [];
            connection.execSql(request);
            await request.on('row', function (columns) {
                columns.forEach(function (column) {
                    if (column.value === null) {
                        console.log('NULL');
                    } else {
                        data[column.metadata.colName] = column.value;
                    }
                });
                result.push(data);
                data = {};
            });

            request.on('doneInProc', function (rowCount, more) {
                if (rowCount) {
                    res.status(200).send(result)
                }
                else {
                    msg = "Please try again later";
                    res.status(500).send(msg)
                }
            });

            request.on('requestCompleted', function () {
                connection.close()
            });
        }
    });
}
 */