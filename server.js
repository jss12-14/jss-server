var express = require('express');
var router = express.Router();
var bodyparser = require('body-parser');
var app = express();

app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, content-type, Accept");
  next();
});

app.get('/', function (req, res) {
  res.send("Welcome to Joyson APIs");
});

require('./app/routes/dimension.route.js')(app, router);
require('./app/routes/login.route.js')(app, router);

// Get port from environment and store in express
var port = process.env.PORT || '80';

//Listen on provided PORT
app.listen(port, () => console.log("Server is running on port",port));